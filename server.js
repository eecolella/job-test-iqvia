const { ApolloServer, ApolloError } = require("apollo-server");
const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const uid = require("nanoid");
const adapter = new FileSync("contacts.json");
const db = low(adapter);

db.defaults({
  contacts: []
}).write();

const addDevDelay = (delay = 500) => process.env.IS_DEV_ENV && new Promise(res => setTimeout(res, delay))

const server = new ApolloServer({
  resolvers: {
    Query: {
      async contacts() {
        await addDevDelay();
        return db.get("contacts").value();
      },
      async contact(_, { id }) {
        await addDevDelay();
        return db
          .get("contacts")
          .find({ id })
          .value();
      }
    },
    Mutation: {
      async addContact(_, { contact }) {
        const now = new Date().toISOString();
        let newContact = { ...contact, id: uid(), creationDate: now, modifiedDate: now };
        await db
          .get("contacts")
          .push(newContact)
          .write();
        return newContact;
      },
      async deleteContact(_, { id }) {
        await db
          .get("contacts")
          .remove({ id })
          .write();
        await addDevDelay();
        return true;
      },
      async updateContact(_, { contact }) {
        await db
          .get("contacts")
          .find({ id: contact.id })
          .assign({ ...contact, modifiedDate: new Date().toISOString() })
          .write();

        return db
          .get("contacts")
          .find({ id: contact.id })
          .value();
      }
    }
  },
  typeDefs: `
    type Contact {
      id: ID
      name: String
      email: String
      creationDate: String
      modifiedDate: String
    }

    input InputContact {
      id: ID
      name: String
      email: String
    }

    type Query {
      contacts: [Contact]
      contact(id: ID): Contact
    }

    type Mutation {
      addContact(contact: InputContact): Contact
      deleteContact(id: ID): Boolean
      updateContact(contact: InputContact): Contact
    }
  `
});

server.listen(3001).then(() => {
  console.log("running @ http://localhost:3001");
});
