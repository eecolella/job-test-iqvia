Hi,

To be fair, the test took me quite a bit of time, mainly fighting against Material UI. It took me roughly four hours and I didn't complete all of it.

In conclusion, I found the test very interesting. From the coding point of view, the most challenging thing was to define the boundaries between xstate and apollo client hooks. I'm definitely curious to see how xstate scales in complex applications!

Thanks,

Ermes
