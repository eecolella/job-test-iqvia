export interface Contact {
  name: string;
  email: string;
  id: string;
  creationDate?: string;
  modifiedDate?: string;
}
