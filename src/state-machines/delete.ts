import { Machine, EventObject, actions } from 'xstate';

export enum DeleteStates {
  IDLE = 'idle',
  OPEN = 'open',
  DELETING = 'deleting',
}

export enum DeleteEventTypes {
  CLOSE = 'CLOSE',
  OPEN = 'OPEN',
  CONFIRM = 'CONFIRM',
}

interface DeleteStateSchema {
  states: {
    [DeleteStates.IDLE]: {};
    [DeleteStates.OPEN]: {};
    [DeleteStates.DELETING]: {};
  };
}

interface DeleteContext {
  selectedId?: string;
}

interface DeleteEvent extends EventObject {
  type: DeleteEventTypes;
  id?: string;
}

const selectId = actions.assign<DeleteContext, DeleteEvent>((context, event) => ({
  selectedId: event.id,
}));

const deleteMachine = Machine<DeleteContext, DeleteStateSchema, DeleteEvent>({
  id: 'delete',
  initial: DeleteStates.IDLE,
  context: {
    selectedId: undefined,
  },
  states: {
    [DeleteStates.IDLE]: {
      on: {
        [DeleteEventTypes.OPEN]: {
          target: DeleteStates.OPEN,
          actions: 'selectId',
        },
      },
    },
    [DeleteStates.OPEN]: {
      on: {
        [DeleteEventTypes.CLOSE]: DeleteStates.IDLE,
        [DeleteEventTypes.CONFIRM]: DeleteStates.DELETING,
      },
    },
    [DeleteStates.DELETING]: {
      on: {
        [DeleteEventTypes.CLOSE]: DeleteStates.IDLE,
      },
    },
  },
}, {
  actions: {selectId},
});

export default deleteMachine
