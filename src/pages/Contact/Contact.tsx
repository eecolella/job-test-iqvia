import React from 'react';
import {
  Avatar,
  ListItemSecondaryAction,
  Card,
  CardHeader,
  Typography,
  Divider,
  Box,
  Grid,
  CircularProgress,
  IconButton
} from '@material-ui/core';
import {useMachine} from '@xstate/react';
import {RouteComponentProps} from 'react-router-dom';
import {Delete, Edit} from '@material-ui/icons';
import {useQuery, useMutation} from '@apollo/react-hooks';

import DeleteDialog from '../../components/DeleteDialog/DeleteDialog';
import deleteMachine, {DeleteEventTypes, DeleteStates} from '../../state-machines/delete';
import SubTitle from '../../components/SubTitle/SubTitle';
import {queries, mutations} from '../../graphql';
import {Contact as IContact} from '../../types';
import BackButton from '../../components/BackButton/BackButton';

import './Contact.css';

export interface ContactRouteParams {
  id: string
}

const Contact: React.FunctionComponent<RouteComponentProps<ContactRouteParams>> = (props) => {
  const {id} = props.match.params;

  const {loading, error, data} = useQuery<{ contact: IContact }>(queries.GET_CONTACT, {variables: {id: id}});
  const [deleteContact] = useMutation(mutations.DELETE_CONTACT);

  const [smDeleteCurrent, smDeleteSend] = useMachine(deleteMachine);

  if (loading) return <Grid container justify='center'><Box p={2}><CircularProgress color="secondary"/></Box></Grid>;
  if (error || !data || !data.contact) return <>Error: {error}</>;

  const closeHandler = () => {
    smDeleteSend(DeleteEventTypes.CLOSE);
  };

  const deleteHandler = (id: string) => () => {
    smDeleteSend(DeleteEventTypes.OPEN, {id});
  };

  const editHandler = (id: string) => () => {
    props.history.push(`/contact/${id}/edit`);
  };

  const confirmHandler = async () => {
    smDeleteSend(DeleteEventTypes.CONFIRM);
    try {
      await deleteContact({variables: {id: smDeleteCurrent.context.selectedId}});
      props.history.push('/');
    } catch (e) {
      // todo: show an error message with mui.Snackbar
    }
  };

  const backHandler = () => {
    props.history.push('/');
  };

  const {name, email, creationDate, modifiedDate} = data.contact;

  return (
    <>
      <SubTitle>{name}</SubTitle>
      <BackButton onClick={backHandler}/>
      <Card>
        <Grid container justify='flex-start'>
          <Grid item>
            <CardHeader
              avatar={
                <Avatar>
                  {name[0].toUpperCase()}
                </Avatar>
              }
              title={`Name: ${name}`}
              subheader={`E-mail: ${email}`}
            />
          </Grid>
          <Grid item className="card-actions">
            <ListItemSecondaryAction>
              <IconButton color="secondary" onClick={editHandler(id)}>
                <Edit/>
              </IconButton>
              <IconButton color="secondary" onClick={deleteHandler(id)}>
                <Delete/>
              </IconButton>
            </ListItemSecondaryAction>
          </Grid>
        </Grid>
        <Divider variant='fullWidth'/>
        <Box p={2}>
          <Grid container justify='space-evenly'>
            <Typography variant="body2">
              Created: {creationDate && new Date(creationDate).toLocaleString()}
            </Typography>
            <Typography variant="body2">
              Modified: {modifiedDate && new Date(modifiedDate).toLocaleString()}
            </Typography>
          </Grid>
        </Box>
      </Card>
      <DeleteDialog
        isOpen={smDeleteCurrent.matches(DeleteStates.OPEN)}
        closeHandler={closeHandler}
        confirmHandler={confirmHandler}
      />
    </>
  )
};

export default Contact;
