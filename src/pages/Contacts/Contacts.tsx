import React from 'react';
import {useQuery, useMutation} from '@apollo/react-hooks';
import {Box, Grid, Button, CircularProgress} from '@material-ui/core';
import {Add} from '@material-ui/icons';
import {RouteComponentProps} from 'react-router-dom';
import {useMachine} from '@xstate/react';

import {queries, mutations} from '../../graphql';
import {Contact} from '../../types';
import deleteMachine, {DeleteEventTypes, DeleteStates} from '../../state-machines/delete';
import DeleteDialog from '../../components/DeleteDialog/DeleteDialog';
import SubTitle from '../../components/SubTitle/SubTitle';
import ContactList from '../../components/ContactList/ContactList';

const Contacts: React.FunctionComponent<RouteComponentProps> = (props) => {

  const {loading, error, data, refetch} = useQuery<{ contacts: Contact[] }>(queries.GET_CONTACTS, {fetchPolicy: 'cache-and-network'});
  const [deleteContact] = useMutation(mutations.DELETE_CONTACT);

  const [smDeleteCurrent, smDeleteSend] = useMachine(deleteMachine);

  if (loading) return <Grid container justify='center'><Box p={2}><CircularProgress color="secondary"/></Box></Grid>;
  if (error || !data || !data.contacts) return <>Error: {error}</>;

  const closeHandler = () => {
    smDeleteSend(DeleteEventTypes.CLOSE);
  };

  const confirmHandler = async () => {
    smDeleteSend(DeleteEventTypes.CONFIRM);
    try {
      await deleteContact({variables: {id: smDeleteCurrent.context.selectedId}});
      await refetch();
    } catch (e) {
      // todo: show an error message with mui.Snackbar
    }
    smDeleteSend(DeleteEventTypes.CLOSE);
    // todo: show a success message with mui.Snackbar
  };

  const addHandler = () => {
    props.history.push('/contact/add');
  };

  const editHandler = (id: string) => () => {
    props.history.push(`/contact/${id}/edit`);
  };

  const viewHandler = (id: string) => () => {
    props.history.push(`/contact/${id}`);
  };

  const deleteHandler = (id: string) => () => {
    smDeleteSend(DeleteEventTypes.OPEN, {id});
  };

  return <>
    <SubTitle>Contacts</SubTitle>
    <Box mb={1}>
      <Grid container justify='flex-end'>
        <Button variant="outlined" size="medium" color="secondary" onClick={addHandler}>
          Add new&nbsp;&nbsp;<Add/>
        </Button>
      </Grid>
    </Box>
    <ContactList
      contacts={data.contacts}
      isDeletingState={smDeleteCurrent.matches(DeleteStates.DELETING)}
      selectedId={smDeleteCurrent.context.selectedId}
      deleteHandler={deleteHandler}
      editHandler={editHandler}
      viewHandler={viewHandler}
    />
    <DeleteDialog
      isOpen={smDeleteCurrent.matches(DeleteStates.OPEN)}
      closeHandler={closeHandler}
      confirmHandler={confirmHandler}
    />
  </>;
};

export default Contacts;
