import React from 'react';
import {Typography} from '@material-ui/core';
import {RouteComponentProps} from 'react-router-dom';

import SubTitle from '../../components/SubTitle/SubTitle';
import BackButton from '../../components/BackButton/BackButton';

export interface ContactRouteParams {
  id: string
}

const EditContact: React.FunctionComponent<RouteComponentProps<ContactRouteParams>> = (props) => {

  const backHandler = () => {
    props.history.push('/');
  };

  return (
    <>
      <SubTitle>Edit contact</SubTitle>

      <BackButton onClick={backHandler}/>
      <Typography variant="h6" color="secondary">Todo! Please read the notes in the README.md</Typography>
    </>
  )
};

export default EditContact;
