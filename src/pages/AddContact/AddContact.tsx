import React from 'react';
import {Typography} from '@material-ui/core';
import {RouteComponentProps} from 'react-router-dom';

import SubTitle from '../../components/SubTitle/SubTitle';
import BackButton from '../../components/BackButton/BackButton';

const AddContact: React.FunctionComponent<RouteComponentProps> = (props) => {

  const backHandler = () => {
    props.history.push('/');
  };

  return (
    <>
      <SubTitle>Add new contact</SubTitle>
      <BackButton onClick={backHandler}/>
      <Typography variant="h6" color="secondary">Todo! Please read the notes in the README.md</Typography>
    </>
  )
};

export default AddContact;
