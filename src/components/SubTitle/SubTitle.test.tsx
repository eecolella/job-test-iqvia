import React from 'react'
import {render, cleanup} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import SubTitle from './SubTitle'

const text = 'Some text';

afterEach(cleanup);

const setup = () => {
  const utils = render(<SubTitle>{text}</SubTitle>);
  const element = utils.getByText(text);
  return {
    element,
    ...utils,
  }
};

describe('SubTitle', () => {
  it('should render with expected text', () => {
    const {element} = setup();
    expect(element).toHaveTextContent(text);
  });
});
