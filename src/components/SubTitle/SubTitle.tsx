import React from 'react';
import {Box, Typography} from '@material-ui/core';


const SubTitle: React.FunctionComponent = (props) => (
  <Box mb={2}>
    <Typography variant="h4" color="textSecondary" align="center">
      {props.children}
    </Typography>
  </Box>

);

export default SubTitle;
