import React from 'react'
import {render, cleanup} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import ContactList from './ContactList'
import {Contact} from '../../types';

const viewHandler = jest.fn();
const editHandler = jest.fn();
const deleteHandler = jest.fn();
const selectedId = 'another id';
const isDeletingState = false;
const contacts = [
  {
    id: 'some id',
    name: 'some name',
    email: 'some email',
  }
];

afterEach(() => {
  cleanup();
  jest.resetAllMocks();
});

const setup = (contacts: Contact[]) => {
  const utils = render(
    <ContactList
      contacts={contacts}
      viewHandler={viewHandler}
      editHandler={editHandler}
      deleteHandler={deleteHandler}
      selectedId={selectedId}
      isDeletingState={isDeletingState}
    />
  );

  const element = utils.getByTestId('contact-list');

  let noContacts, firstContactItem;

  try {
    noContacts = utils.getByTestId('no-contacts-message');
  } catch (e) {
  }

  try {
    firstContactItem = utils.getByTestId('contact-item');
  } catch (e) {
  }

  return {
    element,
    noContacts,
    firstContactItem,
    ...utils,
  }
};

describe('ContactList', () => {
  it('should render without crashing', () => {
    const {element} = setup([]);
    expect(element).toBeInTheDocument();
  });

  describe('when there are not contacts', () => {
    it('should render the appropriate message', () => {
      const {noContacts} = setup([]);
      expect(noContacts).toBeInTheDocument();
    });
  });

  describe('when there are some contacts', () => {
    it('should render the contacts', () => {
      const {firstContactItem} = setup(contacts);
      expect(firstContactItem).toBeInTheDocument();
    });
  })
});
