import React from 'react';
import {List, Typography, Grid} from '@material-ui/core';

import {Contact} from '../../types';
import ContentItem from '../ContactItem/ContactItem';

export interface ContactListProps {
  contacts: Contact[];
  isDeletingState: boolean;
  selectedId?: string;
  viewHandler: (id: string) => () => void;
  editHandler: (id: string) => () => void;
  deleteHandler: (id: string) => () => void;
}

const ContactList: React.FunctionComponent<ContactListProps> = (props) => (
  <List dense data-testid="contact-list">
    {
      props.contacts.map(({name, email, id}) => (
        <ContentItem
          key={id}
          id={id}
          name={name}
          email={email}
          isDeletingState={props.isDeletingState}
          selectedId={props.selectedId}
          viewHandler={props.viewHandler}
          editHandler={props.editHandler}
          deleteHandler={props.deleteHandler}
        />
      ))
    }
    {
     !props.contacts.length && (
       <Grid container justify='center' data-testid="no-contacts-message">
         <Typography color="textSecondary">
           There are no contacts. Try to create some!
         </Typography>
       </Grid>
     )
    }
  </List>
);

export default ContactList;
