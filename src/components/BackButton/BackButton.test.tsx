import React from 'react'
import {render, cleanup, fireEvent} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import BackButton from './BackButton'

const clickHandler = jest.fn();

afterEach(() => {
  cleanup();
  jest.resetAllMocks();
});

const setup = () => {
  const utils = render(<BackButton onClick={clickHandler} />);
  const element = utils.getByRole('button');
  return {
    element,
    ...utils,
  }
};

describe('BackButton', () => {
  it('should render without crashing', () => {
    const {element} = setup();
    expect(element).toBeInTheDocument();
    expect(clickHandler).not.toBeCalled();
  });

  it('should call the click handler when clicked', () => {
    const {element} = setup();
    fireEvent.click(element);
    expect(clickHandler).toBeCalled();
  });
});
