import React, {MouseEvent} from 'react';
import {Box, Grid, Button} from '@material-ui/core';
import {ArrowBack} from '@material-ui/icons';

export interface BackButtonProps {
  onClick: (event: MouseEvent) => void;
}


const BackButton: React.FunctionComponent<BackButtonProps> = (props) => (
  <Box mb={2}>
    <Grid container justify='flex-start'>
      <Button variant="outlined" size="medium" color="secondary" onClick={props.onClick}>
        <ArrowBack/>&nbsp;&nbsp;Back to Contacts
      </Button>
    </Grid>
  </Box>

);

export default BackButton;
