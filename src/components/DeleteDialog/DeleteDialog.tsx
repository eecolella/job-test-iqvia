import React, {MouseEvent} from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, DialogContentText} from '@material-ui/core';

export interface DeleteDialogProps {
  isOpen: boolean;
  closeHandler: (event: MouseEvent) => void;
  confirmHandler: (event: MouseEvent) => void;
}

const DeleteDialog: React.FunctionComponent<DeleteDialogProps> = (props) => (
  <Dialog
    open={props.isOpen}
    onClose={props.closeHandler}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">Do you confirm you want to delete this contact?</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        This action is not reversible.
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={props.closeHandler} color="secondary" data-testid="delete-dialog__cancel">
        Cancel
      </Button>
      <Button onClick={props.confirmHandler} color="secondary" autoFocus data-testid="delete-dialog__confirm">
        Confirm
      </Button>
    </DialogActions>
  </Dialog>

);

export default DeleteDialog;
