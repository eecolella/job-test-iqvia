import React from 'react'
import {render, cleanup, fireEvent} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import DeleteDialog from './DeleteDialog'

const closeHandler = jest.fn();
const confirmHandler = jest.fn();

afterEach(() => {
  cleanup();
  jest.resetAllMocks();
});

const setup = (isOpen: boolean) => {
  const utils = render(
    <DeleteDialog
      closeHandler={closeHandler}
      confirmHandler={confirmHandler}
      isOpen={isOpen}
    />
  );
  let element, cancelButton, confirmButton;
  try {
    element = utils.getByRole('dialog');
    cancelButton = utils.getByTestId('delete-dialog__cancel');
    confirmButton = utils.getByTestId('delete-dialog__confirm');
  } catch (e) {
  }

  return {
    element,
    cancelButton,
    confirmButton,
    ...utils,
  }
};

describe('DeleteDialog', () => {
  describe('when isOpen is false', () => {
    it('should not render anything', () => {
      const {element} = setup(false);
      expect(element).toBeUndefined();
    });
  });

  describe('when isOpen is true', () => {
    it('should render the dialog', () => {
      const {element} = setup(true);
      expect(element).toBeInTheDocument();
    });

    it('should call the close handler when the cancel button is clicked', () => {
      const {cancelButton} = setup(true);
      fireEvent.click(cancelButton as Element);
      expect(closeHandler).toBeCalled();
      expect(confirmHandler).not.toBeCalled();
    });

    it('should call the confirm handler when the confirm button is clicked', () => {
      const {confirmButton} = setup(true);
      fireEvent.click(confirmButton as Element);
      expect(confirmHandler).toBeCalled();
      expect(closeHandler).not.toBeCalled();
    });
  })
});
