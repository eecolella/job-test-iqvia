import React from 'react';
import { Grid, Typography } from '@material-ui/core';

const Layout: React.FunctionComponent = props =>
  (
    <div>
      <header className="App-header">
          <Typography variant="h1">Contacts Manager</Typography>
      </header>
      <Grid container justify='center'>
        <Grid item xs={12} md={6}>
          {props.children}
        </Grid>
      </Grid>
    </div>
  );

export default Layout;
