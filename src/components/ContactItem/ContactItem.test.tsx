import React from 'react'
import {render, cleanup, fireEvent} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import ContactItem from './ContactItem'

const viewHandler = jest.fn();
const editHandler = jest.fn();
const deleteHandler = jest.fn();
const id = 'some id';
const name = 'some name';
const email = 'some email';

afterEach(() => {
  cleanup();
  jest.resetAllMocks();
});

const setup = (isDeletingState: boolean, selectedId?: string) => {
  const utils = render(
    <ContactItem
      id={id}
      name={name}
      email={email}
      viewHandler={viewHandler}
      editHandler={editHandler}
      deleteHandler={deleteHandler}
      selectedId={selectedId}
      isDeletingState={isDeletingState}
    />
  );

  const element = utils.getByTestId('contact-item');
  let spinner, editButton, deleteButton, viewButton;

  try {
    spinner = utils.getByRole('progressbar');
  } catch (e) {
  }

  try {
    editButton = utils.getByTestId('contact-item__edit');
    deleteButton = utils.getByTestId('contact-item__delete');
    viewButton = utils.getByTestId('contact-item__view');
  } catch (e) {
  }

  return {
    element,
    spinner,
    editButton,
    deleteButton,
    viewButton,
    ...utils,
  }
};

describe('ContactItem', () => {
  it('should render without crashing', () => {
    const {element, spinner} = setup(false);
    expect(element).toBeInTheDocument();
    expect(spinner).toBeUndefined()
  });

  describe('when or is not in the deleting state or is not the selected id', () => {
    it('should render the name', () => {
      const {element, spinner} = setup(false);
      expect(element).toHaveTextContent(name);
      expect(spinner).toBeUndefined()
    });

    it('should render the email', () => {
      const {element, spinner} = setup(false);
      expect(element).toHaveTextContent(email);
      expect(spinner).toBeUndefined()
    });

    it('should call the edit handler when the edit button is clicked', () => {
      const {editButton} = setup(true);
      fireEvent.click(editButton as Element);
      expect(editHandler).toBeCalled();
    });

    it('should call the view handler when the view button is clicked', () => {
      const {viewButton} = setup(true);
      fireEvent.click(viewButton as Element);
      expect(viewHandler).toBeCalled();
    });

    it('should call the delete handler when the delete button is clicked', () => {
      const {deleteButton} = setup(true);
      fireEvent.click(deleteButton as Element);
      expect(deleteHandler).toBeCalled();
    });
  });

  describe('when is in the deleting state and is the selected id', () => {
    it('should render the spinner', () => {
      const {element, spinner} = setup(true, id);
      expect(element).not.toHaveTextContent(name);
      expect(spinner).toBeInTheDocument()
    });
  })
});
