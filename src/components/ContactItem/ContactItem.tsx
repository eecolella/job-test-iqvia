import React from 'react';
import {
  Box,
  Paper,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Avatar,
  IconButton,
  Grid,
  CircularProgress
} from '@material-ui/core';
import {Delete, Edit} from '@material-ui/icons';

import './ContactItem.css';
import {Contact} from '../../types';

export interface ContentItemProps extends Contact{
  isDeletingState: boolean;
  selectedId?: string;
  viewHandler: (id: string) => () => void;
  editHandler: (id: string) => () => void;
  deleteHandler: (id: string) => () => void;
}

const ContentItem: React.FunctionComponent<ContentItemProps> = (props) => (
  <Box mb={2} data-testid="contact-item">
    <Paper elevation={2}>
      {
        props.isDeletingState && props.id === props.selectedId
          ? <Grid container justify='center'><Box p={1}><CircularProgress color="secondary"/></Box></Grid>
          : (
            <ListItem className='list-item' onClick={props.viewHandler(props.id)} data-testid="contact-item__view">
              <ListItemAvatar>
                <Avatar aria-label="avatar">
                  {props.name[0].toUpperCase()}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={props.name}
                secondary={props.email}
              />
              <ListItemSecondaryAction>
                <IconButton color="secondary" onClick={props.editHandler(props.id)} data-testid="contact-item__edit">
                  <Edit/>
                </IconButton>
                <IconButton color="secondary" onClick={props.deleteHandler(props.id)} data-testid="contact-item__delete">
                  <Delete/>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          )
      }
    </Paper>
  </Box>
);

export default ContentItem;
