import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter } from 'react-router-dom';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import './App.css';
import { Routes } from './routes';

export const client = new ApolloClient({
  uri: 'http://localhost:3001',
});

export const theme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});


const App: React.FunctionComponent = () =>
  (
    <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <BrowserRouter basename='/' children={Routes} />
    </ThemeProvider>
    </ApolloProvider>
  );


export default App;
