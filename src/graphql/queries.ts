import { gql } from 'apollo-boost';

export const GET_CONTACTS = gql`query getContacts{
    contacts {
        id,
        name,
        email
    }
}`;

export const GET_CONTACT = gql`query getContact($id: ID){
    contact(id: $id){
        id,
        name,
        email,
        modifiedDate,
        creationDate
    }
}`;
