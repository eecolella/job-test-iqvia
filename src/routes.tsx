import React from 'react';
import {Route, Switch} from 'react-router-dom';

import Layout from './components/Layout/Layout';
import Contacts from './pages/Contacts/Contacts';
import Contact from './pages/Contact/Contact';
import AddContact from './pages/AddContact/AddContact';
import EditContact from './pages/EditContact/EditContact';

export const Routes = (
  <Layout>
    <Switch>
      <Route component={Contacts} path='/' exact/>
      <Route component={EditContact} path='/contact/:id/edit'/>
      <Route component={AddContact} path='/contact/add'/>
      <Route component={Contact} path='/contact/:id'/>
    </Switch>
  </Layout>
);
